source platform/ci/before-script.sh
echo "Building" $NAME
cd platform/ci/docker/custom-image
ls -la
docker build . -f Dockerfile.gitlab -t myd-gitlab
docker build . -f Dockerfile.nexus -t myd-nexus
docker build . -f Dockerfile.traefik -t myd-traefik
docker build . -f Dockerfile.kibana -t myd-kibana
docker build . -f Dockerfile.elasticsearch -t myd-elasticsearch
cd filebeat
docker build . -f Dockerfile.filebeat -t myd-filebeat:0.1
cd ..
docker compose -f ../compose/build.yml up -d